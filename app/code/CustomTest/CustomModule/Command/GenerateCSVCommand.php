<?php

namespace CustomTest\CustomModule\Command;

use CustomTest\CustomModule\Model\CustomModel;
use Symfony\Component\Console\Command\Command;
use Magento\Framework\App\Filesystem\DirectoryList;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateCSVCommand extends Command
{
    public function __construct(private readonly CustomModel $customModel, private readonly DirectoryList $directoryList) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('custommodule:generate:csv');
        $this->setDescription('Generate a CSV file containing all data from the custom module table');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $data = $this->customModel->getCollection()->getData();

        if (empty($data)) {
            $io->error('No data available to generate CSV.');
            return Command::FAILURE;
        }

        $csvFilePath = $this->directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'custom_module_export.csv';

        $csvFile = fopen($csvFilePath, 'w');
        if ($csvFile === false) {
            $io->error('Failed to open the CSV file for writing.');
            return Command::FAILURE;
        }

        $header = array_keys(reset($data));
        fputcsv($csvFile, $header);

        foreach ($data as $row) {
            fputcsv($csvFile, $row);
        }

        fclose($csvFile);

        $io->success('CSV file generated successfully at ' . $csvFilePath);

        return Command::SUCCESS;
    }
}
