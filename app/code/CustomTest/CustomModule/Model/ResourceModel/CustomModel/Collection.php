<?php

namespace CustomTest\CustomModule\Model\ResourceModel\CustomModel;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'customtest_custommodule_custommodel_collection';
    protected $_eventObject = 'custommodel_collection';

    protected function _construct(): void
    {
        $this->_init(
            'CustomTest\CustomModule\Model\CustomModel',
            'CustomTest\CustomModule\Model\ResourceModel\CustomModel'
        );
    }
}
