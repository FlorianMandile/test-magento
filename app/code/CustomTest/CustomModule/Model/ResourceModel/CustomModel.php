<?php

namespace CustomTest\CustomModule\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CustomModel extends AbstractDb
{
    protected function _construct(): void
    {
        $this->_init('custom_module', 'id');
    }
}
