<?php

namespace CustomTest\CustomModule\Model;

use Magento\Framework\Model\AbstractModel;

class CustomModel extends AbstractModel
{
    protected function _construct(): void
    {
        $this->_init('CustomTest\CustomModule\Model\ResourceModel\CustomModel');
    }
}
