<?php

namespace CustomTest\CustomModule\Api;

interface CustomInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function create(mixed $data): array;

    /**
     * @param string $id
     * @return mixed
     */
    public function get(string $id): mixed;

    /**
     * @return mixed
     */
    public function getCollection(): mixed;

    /**
     * @param int $id
     * @param mixed $data
     * @return mixed
     */
    public function update(int $id, mixed $data): mixed;

    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id): mixed;
}
