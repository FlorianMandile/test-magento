<?php

namespace CustomTest\CustomModule\Api;

use CustomTest\CustomModule\Model\CustomModel;

class CustomApi implements CustomInterface
{
    public function __construct(protected CustomModel $customModel)
    {
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(mixed $data): array
    {
        try {
            $object = $this->customModel->setData($data);
            $object->save();

            return ['success' => true, 'message' => 'Object has been created'];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function get(string $id): mixed
    {
        try {
            $object = $this->customModel->load($id)->toArray();

            return ['success' => true, 'object' => $object];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * @return mixed
     */
    public function getCollection(): mixed
    {
        try {
            $objects = $this->customModel->getCollection()->toArray();

            return ['success' => true, $objects];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(int $id, mixed $data): mixed
    {
        try {
            $object = $this->customModel->load($id);

            $object->addData($data);
            $object->save();

            return ['success' => true, 'message' => 'Object has been updated'];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id): mixed
    {
        try {
            $object = $this->customModel->load($id);

            $object->delete();

            return ['success' => true, 'message' => 'Object has been deleted'];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }
}
