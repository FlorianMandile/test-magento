<?php
namespace CustomTest\CustomModule\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()->newTable(
            $installer->getTable('custom_module')
        )
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'first_name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'First Name'
            )
            ->addColumn(
                'last_name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Last Name'
            )
            ->addColumn(
                'email',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'unique' => true],
                'Email'
            )
            ->addColumn(
                'loyalty_card_number',
                Table::TYPE_TEXT,
                13,
                ['nullable' => false, 'unique' => true],
                'Loyalty Card Number'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Created At'
            )
            ->addColumn(
                'status',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => '1'],
                'Status'
            )
            ->setComment('Custom Module Table');

        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
