# Module Personnalisé pour Magento 2

Ajout d'un module personnalisé avec des champs spécifiques et des fonctionnalités CRUD (Create, Read, Update, Delete) accessibles via API. API. Le module inclut également une commande CLI pour
exporter les données en CSV.
